import React from "react";

const Card = (props) => {
    return (
        <div className="card">
            <div className="card-image">
                <img src={props.image} alt="" />
            </div>
            <div className="card-content">
                <div className="description">
                    <div className="description-title">
                        <h2>Pizza tradicional</h2>
                        <h3>Liru Siza - 14-Feb</h3>
                    </div>
                    <div className="description-stars" aria-label="Rating of this product is 2.3 out of 5."></div>
                </div>
            </div>
        </div>
    );
};

export default Card;