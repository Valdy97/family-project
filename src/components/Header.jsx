import React, { useState } from "react";
import { NavLink } from 'react-router-dom';
import menu from '../assets/icons/icon_menu.svg';

const Header = () => {
	const [toggle, setToggle] = useState(false);

	const handleToggle = () => {
		setToggle(!toggle);
	}
    return (
        <nav>
            <img src={menu} alt="menu" className="menu" />
			<div className="navbar-left">
				<span>Valdiviesos</span>
			</div>
			<div className="navbar-center">
				<ul>
					<li>
						<NavLink 
							className={({ isActive }) => (isActive ? 'navbar__link navbar__link--active' : 'navbar__link')}
							to="/">
							Resumen
						</NavLink>
					</li>
					<li>
						<NavLink 
							className={({ isActive }) => (isActive ? 'navbar__link navbar__link--active' : 'navbar__link')}
							to="/Comida">
							Comida
						</NavLink>
					</li>
					<li>
						<NavLink 
							className={({ isActive }) => (isActive ? 'navbar__link navbar__link--active' : 'navbar__link')}
							to="/Peticiones">
							Mis Peticiones
						</NavLink>
					</li>
				</ul>
			</div>
			<div className="navbar-right">
				<ul>
					<li className="navbar-email" onClick={handleToggle}>
						Valentin
					</li>
				</ul>
			</div>
        </nav>
    );
};

export default Header;