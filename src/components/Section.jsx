import React from "react";

const Section = (props) => {
    const { titulo, children, buttons } = props;
    return (
        <div className="section">
        <div className="section-header">
            <div className="section-title">
                <span>{titulo}</span>
            </div>
            <div className="section-button">
                {buttons && buttons.map(but => (
                    <button className="button button-primary" key={but.label} onClick={but.handler}>
                        <i className={but.icon} />                        
                    {but.label}
                    </button>
                ))}
            </div>
        </div>

        <div className="section-body">
            {children}
        </div>
    </div>
    )
};

export default Section;