import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from '../components/Header';
import Comida from '../pages/Comida';
import Home from '../pages/Home';
import Peticiones from '../pages/Peticiones';
import '../styles/Main.scss';

function App() {
  return (
    <React.Fragment>
			<BrowserRouter>
        <Header />
        <Routes>
          <Route 
              exact 
              path="/" 
              element={<Home />} />
            <Route 
              exact 
              path="/Comida" 
              element={<Comida />} />
            <Route 
              exact 
              path="/Peticiones" 
              element={<Peticiones />} />
        </Routes>
			</BrowserRouter>
    </React.Fragment>
  );
}

export default App;
