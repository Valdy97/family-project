import React from "react";
import Section from "../components/Section";

const Peticiones = () => {
    const handlerNewRequest = () => {
        console.log('holaaaa');
    };
    const buttonsSection = [
        {
            label: 'Nueva petición',
            handler: handlerNewRequest,
            icon: 'fa-solid fa-plus'
        }
    ];
    return(
        <React.Fragment>
            <Section titulo={"Todas las peticiones"} buttons={buttonsSection}>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Petición
                            </th>
                            <th>
                                Precio
                            </th>
                            <th>
                                Lugar / Persona
                            </th>
                            <th>
                                Fecha estimada
                            </th>
                            <th>
                                Dueño
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Celular
                            </td>
                            <td>
                                $5,000
                            </td>
                            <td>
                                Amazon
                            </td>
                            <td>
                                28-Feb
                            </td>
                            <td>
                                Papá
                            </td>
                        </tr>
                    </tbody>
                </table>
            </Section>
        </React.Fragment>
    );
};

export default Peticiones;