import React from "react";
import Section from "../components/Section";
import Card from "../components/Card";

const Comida = () => {
  return (
      <React.Fragment>
            <h1 className="text-center">Turno de Mamá</h1>
            <Section titulo={"Comidas previas"}>
                <Card image="https://images.unsplash.com/photo-1530651788726-1dbf58eeef1f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=882&q=80"/>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Comida
                            </th>
                            <th>
                                Fecha
                            </th>
                            <th>
                                Lugar
                            </th>
                            <th>
                                Truno
                            </th>
                            <th>
                                Calificación
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Hamburguesas
                            </td>
                            <td>
                                Ayer
                            </td>
                            <td>
                                Mc'burguer
                            </td>
                            <td>
                                Mamá
                            </td>
                            <td>
                                <div className="stars" aria-label="Rating of this product is 2.3 out of 5."></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </Section>

      </React.Fragment>
  );
};

export default Comida;