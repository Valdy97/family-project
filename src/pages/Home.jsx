import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Card from "../components/Card";
import Section from "../components/Section";

const Home = () => {
    const onChange = () => {
        console.log('change');
    };

    const onClickItem = () => {
        console.log('onClickItem');
    };

    const onClickThumb = () => {
        console.log('onClickThumb');
    };

    return (
        <React.Fragment>

            <Section titulo={"Turno de comida"}>
                <div className="turnos text-center">
                    <div>
                        <h3>Truno actual:</h3> Valentin
                    </div>
                    <div>
                        <h3>Siguente turno:</h3> Mamá
                    </div>
                </div>
                
                <Carousel autoPlay={true} infiniteLoop={true} showArrows={true} onChange={onChange} onClickItem={onClickItem} onClickThumb={onClickThumb}>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1559386484-97dfc0e15539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80"/>
                        <img src="https://images.unsplash.com/photo-1559386484-97dfc0e15539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80" alt="" />
                    </div>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1533461502717-83546f485d24?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=60"/>
                        <img src="https://images.unsplash.com/photo-1533461502717-83546f485d24?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=60" alt="" />
                    </div>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1530651788726-1dbf58eeef1f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=882&q=80"/>
                        <img src="https://images.unsplash.com/photo-1530651788726-1dbf58eeef1f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=882&q=80" alt="" />
                    </div>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1559386484-97dfc0e15539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80"/>
                        <img src="https://images.unsplash.com/photo-1559386484-97dfc0e15539?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80" alt="" />
                    </div>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1533461502717-83546f485d24?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=60"/>
                        <img src="https://images.unsplash.com/photo-1533461502717-83546f485d24?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=60" alt="" />
                    </div>
                    <div>
                        <Card image="https://images.unsplash.com/photo-1530651788726-1dbf58eeef1f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=882&q=80"/>
                        <img src="https://images.unsplash.com/photo-1530651788726-1dbf58eeef1f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=882&q=80" alt="" />

                    </div>
                </Carousel>
            </Section>

            <Section className="peticiones" titulo={"Peticiones"}>
                <div className="peticiones-seleccionadas">
                    <h3>Peticiones seleccionadas</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>
                                    Petición
                                </th>
                                <th>
                                    Precio
                                </th>
                                <th>
                                    Lugar / Persona
                                </th>
                                <th>
                                    Fecha estimada
                                </th>
                                <th>
                                    Dueño
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Celular
                                </td>
                                <td>
                                    $5,000
                                </td>
                                <td>
                                    Amazon
                                </td>
                                <td>
                                    28-Feb
                                </td>
                                <td>
                                    Papá
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div className="peticiones-otras">
                    <h3>Otras peticiones</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>
                                    Petición
                                </th>
                                <th>
                                    Precio
                                </th>
                                <th>
                                    Lugar / Persona
                                </th>
                                <th>
                                    Dueño
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Abrigo
                                </td>
                                <td>
                                    $1,000
                                </td>
                                <td>
                                    C&A
                                </td>
                                <td>
                                    Papá
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Abrigo
                                </td>
                                <td>
                                    $1,000
                                </td>
                                <td>
                                    C&A
                                </td>
                                <td>
                                    Papá
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Abrigo
                                </td>
                                <td>
                                    $1,000
                                </td>
                                <td>
                                    C&A
                                </td>
                                <td>
                                    Papá
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </Section>
        </React.Fragment>
    );
};

export default Home;